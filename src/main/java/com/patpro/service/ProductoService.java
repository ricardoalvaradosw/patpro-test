/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpro.service;

import com.patpro.pojos.Producto;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author RICARDO
 */
public class ProductoService {
    
    public List<Producto> findAll() {
        List<Producto> productos = new ArrayList<>();
        productos.add(new Producto("1","4","Casacas","Casaca con 4 bolsillos Malabar", 99.50, 80, 45.0, 20));
        productos.add(new Producto("2","4","Casacas","Casaca acolchada con borados", 101.50, 60, 68.6, 48));
        productos.add(new Producto("3","4","Casacas","Casaca con capucha y cierres", 119.50, 80, 75.0, 44));
        productos.add(new Producto("4","1","Polos","Polo Manga corta Arizona", 39.95, 80, 11.45, 60));
        productos.add(new Producto("5","1","Polos","Polo Print Manga corta", 39.95, 80, 11.45, 60));
        productos.add(new Producto("6","1","Polos","Polo Manga Corta a rayas", 24.50, 90, 12.0, 65));
        productos.add(new Producto("7","1","Polos","Polo Cuello Camisero con bolsillo", 49.50, 78, 16.7, 20));
        productos.add(new Producto("8","1","Polos","Polo Cuello Piqué", 39.98, 79, 15.0, 45));
        productos.add(new Producto("9","3","Pantalones","Jean Corte Recto - The Royal", 79.95, 45, 40.0, 16));
        productos.add(new Producto("10","3","Pantalones","Jean Rasgado", 99.9, 79, 42.56, 33));
        productos.add(new Producto("11","3","Pantalones","Jean Focalizado SLIM", 99.95, 65, 49.95, 42));
        productos.add(new Producto("12","2","Camisas","Camisa Manga Larga Cuadros con Hoddie", 99.50, 60, 39.0, 28));
        productos.add(new Producto("13","2","Camisas","Camisa Print Slim Fit", 79.50, 77, 40.0, 33));
        productos.add(new Producto("14","2","Camisas","Camisa Minitprint Blanco", 39.50, 44, 29.99, 28));
        productos.add(new Producto("15","2","Camisas","Camisa Manga corta con estampado", 49.50, 39, 28.35, 20));
        return productos;
    }
    
    public List<Producto> findByName(String nombre) 
    {
         List<Producto> productos = findAll();
         List<Producto> result = productos.stream()                
                .filter(producto -> producto.getNombre().toLowerCase().contains(nombre.toLowerCase()))   
                .collect(Collectors.toList());  
         return result;
    }
    
}
