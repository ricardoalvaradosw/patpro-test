/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpro;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 *
 * @author BISMARK
 */
public class Ejemplo3Test {

    private static WebDriver chromeExecutor;

    public Ejemplo3Test() {
    }

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "D:\\Ricardo\\patpro\\seleniumLab\\chromedriver.exe");
        chromeExecutor = new ChromeDriver();
        chromeExecutor.get("http://localhost/patpro/prueba3.html");
    }

    //Criterio de aceptacion: Debe validar que el token ingresado este correcto
    @Test
    public void testValidarToken() {
        //Activa pago por montos
        chromeExecutor.findElement(By.id("radioMonto")).click();
        //Escoge modo de pago Paypal
        chromeExecutor.findElement(By.id("chkPaypal")).click();
        chromeExecutor.findElement(By.id("txtPaypal")).click();
        chromeExecutor.findElement(By.id("txtPaypal")).sendKeys("7000");
        //Escoge modo de pago Trasferencias - Cmac
        chromeExecutor.findElement(By.id("chkCmac")).click();
        chromeExecutor.findElement(By.id("txtCmac")).click();
        chromeExecutor.findElement(By.id("txtCmac")).sendKeys("500");
        //Verificamos la compra
        chromeExecutor.findElement(By.id("btnVerificarCompra")).click();
        //Generamos la compra
        chromeExecutor.findElement(By.id("btnGenerarCompra")).click();
        //Ingresamos y validamos el token
        WebElement valorToken = chromeExecutor.findElement(By.id("txtToken"));
        valorToken.sendKeys("123456");
        chromeExecutor.findElement(By.id("btnValidarToken")).click();
        //Verificamos el estado del div del mensaje de compra exitosa
        WebElement compraExitosa = chromeExecutor.findElement(By.id("divCompraExitosa"));
        try {
            Assert.assertEquals(Boolean.TRUE, compraExitosa.isDisplayed());
            Thread.sleep(5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Criterio de Aceptacion: Debe validar si la suma de procentajes cobertura el total de la compra.
    @Test
    public void testValidaTotalPorcentajes() {
        //Activa pago por porcentajes de cobertura
        chromeExecutor.findElement(By.id("radioPorcentaje")).click();
        //Escoge modo de pago Paypal
        chromeExecutor.findElement(By.id("chk1Visa")).click();
        chromeExecutor.findElement(By.id("rangeVisa")).click();
        chromeExecutor.findElement(By.id("rangeVisa")).sendKeys("1000");
        //Escoge modo de pago Trasferencias - Cmac
        chromeExecutor.findElement(By.id("chk1MasterCard")).click();
        chromeExecutor.findElement(By.id("rangeMasterCard")).click();
        chromeExecutor.findElement(By.id("rangeMasterCard")).sendKeys("1000");
        //Verificamos la compra
        chromeExecutor.findElement(By.id("btnVerificarCompra")).click();
        //Verificamos si el boton de Generar Compra esta activo
        WebElement estadoGenerarCompra = chromeExecutor.findElement(By.id("btnGenerarCompra"));
        try {
            Assert.assertEquals(Boolean.TRUE, estadoGenerarCompra.isEnabled());
            Thread.sleep(5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Criterio de aceptacion: Debe validar que se envio el token
    @Test
    public void tstValidaEnvioToken() {
         //Activa pago por montos
        chromeExecutor.findElement(By.id("radioMonto")).click();
        //Escoge modo de pago Paypal
        chromeExecutor.findElement(By.id("chkMasterCard")).click();
        chromeExecutor.findElement(By.id("txtMasterCard")).click();
        chromeExecutor.findElement(By.id("txtMasterCard")).sendKeys("4000");
        //Escoge modo de pago Trasferencias - Cmac
        chromeExecutor.findElement(By.id("chkBbva")).click();
        chromeExecutor.findElement(By.id("txtBbva")).click();
        chromeExecutor.findElement(By.id("txtBbva")).sendKeys("3500");
        //Verificamos la compra
        chromeExecutor.findElement(By.id("btnVerificarCompra")).click();
        //Generamos la compra
        chromeExecutor.findElement(By.id("btnGenerarCompra")).click();
        //Verificamos el estado del div del mensaje de compra exitosa
        WebElement generacionExitosa = chromeExecutor.findElement(By.id("divGeneracionExitosa"));
        try {
            Assert.assertEquals(Boolean.TRUE, generacionExitosa.isDisplayed());
            Thread.sleep(5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void quit() {
        chromeExecutor.close();
    }

}
