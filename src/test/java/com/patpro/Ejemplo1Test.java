/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patpro;

import com.patpro.pojos.Producto;
import com.patpro.service.ProductoService;
import com.patpro.util.UtilFunctions;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static jdk.nashorn.tools.ShellFunctions.input;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 *
 * @author RICARDO
 */
public class Ejemplo1Test {

    private static WebDriver chromeExecutor;
    private static ProductoService productoService;

    public Ejemplo1Test() {
    }

    @BeforeClass
    public static void setUp() {
        System.setProperty("webdriver.chrome.driver", "D:\\Ricardo\\patpro\\seleniumLab\\chromedriver.exe");
        chromeExecutor = new ChromeDriver();
        chromeExecutor.get("http://localhost/patpro/prueba1.html");
        productoService = new ProductoService();
    }

    // Criterio de aceptacion: Debe mostrarse el loader de carga
    @Test()
    public void testMuestraPantallaCargayTotales() {
        String filtro = "Cam";
        WebElement nombreProducto = chromeExecutor.findElement(By.id("txtNombreProducto"));
        nombreProducto.sendKeys(filtro);
        WebElement btnPrevisualizar = chromeExecutor.findElement(By.id("btnPrevisualizar"));
        WebElement cargando = chromeExecutor.findElement(By.id("loader"));
        btnPrevisualizar.click();
        List<Producto> productos = productoService.findByName(filtro);
        Double startValue = 0.0;
        startValue = productos.stream().map((p) -> new BigDecimal(p.getPrecio() * p.getCantidadVendida()).setScale(2, RoundingMode.HALF_UP).doubleValue()).reduce(startValue, (accumulator, _item) -> accumulator + _item);
      
        try {
            Assert.assertEquals(Boolean.TRUE, cargando.isDisplayed());
            Thread.sleep(3000);
            WebElement totalVentas = chromeExecutor.findElement(By.id("totalVendido"));
            Thread.sleep(2000);
            Assert.assertEquals(Boolean.FALSE, cargando.isDisplayed());
            // Resultados de total deben ser iguales
            Assert.assertEquals("S/. " + startValue, totalVentas.getText());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Si no se selecciona fecha debe mostrarse la fecha desde mes anterior al actual
    @Test
    public void testValidaFecha() {
        WebElement rangoFechas = chromeExecutor.findElement(By.id("reportrange"));
        WebElement spanFechas = rangoFechas.findElement(By.xpath(".//span"));
        Date fechaMesAnterior = UtilFunctions.getLastMonth();
        String fechaAnterior = UtilFunctions.formatDate(fechaMesAnterior);
        String fechaActual = UtilFunctions.formatDate(new Date());
        String fechaEsperada = fechaAnterior + " - " + fechaActual;
        System.out.println(fechaEsperada);
        try {
            Assert.assertEquals(fechaEsperada, spanFechas.getText());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testValidaColumnasReporte() {
        List<WebElement> tableRows = chromeExecutor.findElements(By.id("trHead"));
        String col1 = "", col2 = "", col3 = "", col4 = "", col5 = "", col6 = "", col7 = "", col8 = "";
        for (WebElement singleRow : tableRows) {
            col1 = singleRow.findElement(By.xpath("th[1]")).getText();
            col2 = singleRow.findElement(By.xpath("th[2]")).getText();
            col3 = singleRow.findElement(By.xpath("th[3]")).getText();
            col4 = singleRow.findElement(By.xpath("th[4]")).getText();
            col5 = singleRow.findElement(By.xpath("th[5]")).getText();
            col6 = singleRow.findElement(By.xpath("th[6]")).getText();
            col7 = singleRow.findElement(By.xpath("th[7]")).getText();
            col8 = singleRow.findElement(By.xpath("th[8]")).getText();
        }
        try {
            Assert.assertEquals("Código", col1);
            Assert.assertEquals("Descripción", col2);
            Assert.assertEquals("Categoría", col3);
            Assert.assertEquals("Stock", col4);
            Assert.assertEquals("Precio Compra", col5);
            Assert.assertEquals("Precio Venta", col6);
            Assert.assertEquals("Cantidad Vendida", col7);
            Assert.assertEquals("Total Vendido", col8);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //@Test
    public void validaTotalVentas() {

        // WebElement totalVentas = chromeExecutor.findElement(By.id("totalVendido"));
//        WebElement totalVentas = (WebElement) ((JavascriptExecutor) chromeExecutor).executeScript("return $('#totalVendido').get(0);");
//        System.out.println(totalVentas);
        WebElement nombreProducto = chromeExecutor.findElement(By.id("txtNombreProducto"));
        nombreProducto.sendKeys("Cam");
        WebElement btnPrevisualizar = chromeExecutor.findElement(By.id("btnPrevisualizar"));
        WebElement cargando = chromeExecutor.findElement(By.id("loader"));
        btnPrevisualizar.click();
        try {
            Thread.sleep(1500);
            WebElement totalVentas = chromeExecutor.findElement(By.id("totalVendido"));
            System.out.println(totalVentas);
        } catch (InterruptedException ex) {
            Logger.getLogger(Ejemplo1Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @AfterClass
    public static void quit() {
        chromeExecutor.close();
    }

}
